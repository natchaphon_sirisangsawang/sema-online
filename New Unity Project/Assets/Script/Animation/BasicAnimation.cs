﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Manager;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;

namespace Animation
{
    public class BasicAnimation : MonoBehaviour
    {
        private Animator animBoyM1;
        private Animator animBoyM4;
        private Animator animGirlM1;
        private Animator animGirlM4;
        private Animator animfadeIn;
        private Animator animfadeOut;
        private Animator cameraAnim;
        private bool controleLoop;
        
        public GameObject boyM1;
        public GameObject boyM4;
        public GameObject girlM1;
        public GameObject girlM4;
        public GameObject fadeIn;
        public GameObject fadeOut;
        [SerializeField] private GameObject camera;
        public Button takeTra;

        public event Action HideStory;
        public event Action Fade;

        [SerializeField] private Transform bg_M4;
        [SerializeField] private Transform bg_M1;
        [SerializeField] private Transform EndOfBG_M4;
        [SerializeField] private float speed;
        [SerializeField] private RectTransform fade;

        public static BasicAnimation Instance {get; set; }
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            
            takeTra.gameObject.SetActive(false);
            fade.gameObject.SetActive(!true);
        }

        private void Start()
        {
            controleLoop = false;
            animBoyM1 = boyM1.GetComponent<Animator>();
            animBoyM4 = boyM4.GetComponent<Animator>();
            animGirlM1 = girlM1.GetComponent<Animator>();
            animGirlM4 = girlM4.GetComponent<Animator>();
            animfadeIn = fadeIn.GetComponent<Animator>();
            animfadeOut = fadeOut.GetComponent<Animator>();
            cameraAnim = camera.GetComponent<Animator>();
            takeTra.onClick.AddListener(HandleAnim);
        }
        
        private void HandleAnim()
        {
            HideStory?.Invoke();
            takeTra.gameObject.SetActive(!true);
            animBoyM1.SetBool("IsRunning",true);
            animBoyM4.SetBool("IsRunning",true);
            animGirlM1.SetBool("IsRunning",true);
            animGirlM4.SetBool("IsRunning",true);
            cameraAnim.SetBool("IsStart",true);
            Invoke("BackToIdel",3.5f);
            controleLoop = true;
        }

        private void BackToIdel()
        {
            animBoyM1.SetBool("IsRunning", false);
            animBoyM4.SetBool("IsRunning", false);
            animGirlM1.SetBool("IsRunning", false);
            animGirlM4.SetBool("IsRunning", false);
            controleLoop = false;
            Invoke("FadeIn",3);
        }

        private void FadeIn()
        {
            fade.gameObject.SetActive(true);
            SoundManager.Instance.Play(SoundManager.Sound.Shutter);
            animfadeIn.SetBool("Fade",true);
            Invoke("FadeOut",2);
        }

        private void FadeOut()
        {
            Fade?.Invoke();
            animfadeIn.SetBool("Fade",false);
        }

        private void FixedUpdate()
        {
            if ( controleLoop)
            {
                bg_M4.transform.position = Vector2.MoveTowards(bg_M4.transform.position, EndOfBG_M4.transform.position,
                    speed * Time.deltaTime);
                bg_M1.transform.position = Vector2.MoveTowards(bg_M1.transform.position, EndOfBG_M4.transform.position,
                    speed * Time.deltaTime);
            }
        }

        public void HideCharacter()
        {
            // HideCharacter
            boyM1.gameObject.SetActive(false);
            boyM4.gameObject.SetActive(false);
            girlM1.gameObject.SetActive(false);
            girlM4.gameObject.SetActive(false);
        }
        
    }
}

