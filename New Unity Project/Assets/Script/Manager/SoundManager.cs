using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private SoundClip[] soundClips;

        public static SoundManager Instance { get; private set; }

        public void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            UIManager.ButtonClickedEvent += ButtonClickedSound;
        }

        private void ButtonClickedSound()
        {
            //SoundClick
            Play(Sound.Button);
        }
        
        public enum Sound
        {
            //SoundClip
            Shutter,
            Button
        }

        [Serializable]
        public class SoundClip
        {
            //Init of every SoundClip
            public Sound Sound;
            public AudioClip AudioClip;
            [Range(0, 2)] public float SoundVolume;
            public bool Loop = false;
            [HideInInspector] public AudioSource AudioSource;
        }

        public void Play(Sound sound)
        {
            //Play Sound system
            var soundClip = GetSoundClip(sound);
            if (soundClip.AudioSource == null)
            {
                soundClip.AudioSource = gameObject.AddComponent<AudioSource>();
            }
            soundClip.AudioSource.clip = soundClip.AudioClip;
            soundClip.AudioSource.volume = soundClip.SoundVolume;
            soundClip.AudioSource.loop = soundClip.Loop;
            soundClip.AudioSource.Play();
        }

        private SoundClip GetSoundClip(Sound sound)
        {
            //find sound in list
            foreach (var soundClip in soundClips)
            {
                if (soundClip.Sound == sound)
                {
                    return soundClip;
                }
            }
            return null;
        }
    }
}
