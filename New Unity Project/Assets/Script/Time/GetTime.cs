using System.Collections;
using System.Collections.Generic;
using System.Timers;
using TMPro;
using UnityEngine;

public class GetTime : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI[] date;
    private void Start()
    {
        var time = System.DateTime.UtcNow.ToLocalTime().ToString("วันที่ dd มิถุนายน yyyy");
        print(time);

        date[0].text = time;
        date[1].text = time;
    }
}
