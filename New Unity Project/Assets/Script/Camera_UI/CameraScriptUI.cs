﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using UnityEditor;
using UnityEngine.Networking;


public class CameraScriptUI : MonoBehaviour
{

    public RawImage rawimage;  //Image for rendering what the camera sees.
    WebCamTexture webcamTexture = null;
    public Image img;
    public SpriteRenderer[] faces;
    string imageString;
    
    void Start()
    {
        //Save get the camera devices, in case you have more than 1 camera.
        WebCamDevice[] camDevices = WebCamTexture.devices;
         
        //Get the used camera name for the WebCamTexture initialization.
        string camName = camDevices[0].name;
        webcamTexture = new WebCamTexture(camName, 500, 500, 25);
 
        //Render the image in the screen.
        rawimage.texture = webcamTexture;
        rawimage.material.mainTexture = webcamTexture;
        webcamTexture.Play();
    }

    // ReSharper disable Unity.PerformanceAnalysis
    IEnumerator UploadSaveImage()
    {
        //Create a Texture2D with the size of the rendered image on the screen.
        Texture2D texture = new Texture2D(webcamTexture.width,webcamTexture.height);
        //Save the image to the Texture2D
        texture.SetPixels(webcamTexture.GetPixels());
        //textureCircle.ReadPixels(new Rect(0,0,Screen.width,Screen.height),0,0);
        texture.Apply();
        //Encode it as a PNG.
        byte[] bytes = texture.EncodeToPNG();
        imageString = Convert.ToBase64String(bytes);
        WWWForm form = new WWWForm();
        Debug.Log(imageString.Length);
        form.AddField("image",imageString);
        UnityWebRequest www = UnityWebRequest.Post("https://xn--72crqgjbk3cv2bkye0hsbj8q.com/unity_backend/sendImageToDB.php",form);
        www.chunkedTransfer = false;
        yield return www.SendWebRequest();
        Debug.Log(www.downloadHandler.text);
        Debug.Log(www.error);
    }

    // ReSharper disable Unity.PerformanceAnalysis
    IEnumerator DownloadImagefromDB()
    {
        WWWForm form = new WWWForm();
        form.AddField("savedImage",imageString);
        UnityWebRequest www = UnityWebRequest.Post("https://xn--72crqgjbk3cv2bkye0hsbj8q.com/unity_backend/getImageFromDB.php",form);
        www.chunkedTransfer = false;

        yield return www.SendWebRequest();
        Debug.Log(www.error);
        string fromDBString = www.downloadHandler.text;
        Debug.Log(fromDBString);
        byte[] imageBytesFromDB = Convert.FromBase64String(fromDBString);
        
        Debug.Log(imageBytesFromDB);

        Texture2D newTexture = new Texture2D(1,1);
        newTexture.LoadImage(imageBytesFromDB);
        newTexture.Apply();
        
        Sprite Face = Sprite.Create(newTexture, new Rect(0,0,newTexture.width, newTexture.height), new Vector2(0.5f,0.5f));
        img.sprite = Face;
        foreach (var face in faces)
        {
            face.sprite = Face;
        }
    }

    
    IEnumerator TakePhoto()  // Start this Coroutine on some button click
    {
        yield return new WaitForEndOfFrame();
    }
    
    // ReSharper disable Unity.PerformanceAnalysis


    public void RoutineWrap()
    {
        StartCoroutine(TakePhoto());
        StartCoroutine(UploadSaveImage());
        StartCoroutine(DownloadImagefromDB());
        //webcamTexture.Stop();
    }
    
}

