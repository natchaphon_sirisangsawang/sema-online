﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Manager
{
    public class AppManager : MonoBehaviour
    {
        [Header("Rotation TraSKR")]
        [SerializeField] private Rigidbody traSKR;
        private int[] randomTorque = {10000, -10000};

        private void Start()
        {
            InvokeRepeating("RotateTraSKR",2,10);
        }

        private void RotateTraSKR()
        {
            var random = randomTorque[Random.Range(0, randomTorque.Length)];
            traSKR.AddTorque(0,random,0);
        }
    }
}

