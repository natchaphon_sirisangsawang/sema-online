﻿using System;
using System.Collections.Generic;
using Animation;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class UIManager : MonoBehaviour
    {
        //Panel 1
        [Header("Start Panel")] 
        [SerializeField] private Image bgM1;
        [SerializeField] private Image bgM4;
        [SerializeField] private TextMeshProUGUI head;
        [SerializeField] private RectTransform startPanel;
        [SerializeField] private Button startButton;

        //Panel 2 M1
        [Header("Character M1 Panel")] 
        [SerializeField] private RectTransform chooseCharacterPanelM1;

        [SerializeField] private Image[] charactersM1;
        [SerializeField] private Button nextCharM1;
        [SerializeField] private Button previousCharsM1;
        [SerializeField] private Button nextPageM1;

        //Panel 2 M4
        [Header("Character M4 Panel")] 
        [SerializeField] private RectTransform chooseCharacterPanelM4;

        [SerializeField] private Image[] charactersM4;
        [SerializeField] private Button nextCharM4;
        [SerializeField] private Button previousCharsM4;
        [SerializeField] private Button nextPageM4;

        //Panel 3
        [Header("Photo Panel")] 
        [SerializeField] private RectTransform uploadPhotoPanel;
        [SerializeField] private Button toStoryButton;

        //Panel 4 M1
        [Header("Story M1 Panel")] 
        [SerializeField]
       private RectTransform storyPanelM1;

        //Panel 4 M4
        [Header("Story M4 Panel")]
        [SerializeField] private RectTransform storyPanelM4;
        
        //Dropdown
        [Header("DropDown")] 
        [SerializeField] private TMP_Dropdown dropdown;
        [SerializeField] private TextMeshProUGUI start_text;
        private List<string> grade = new List<string>() {"เลือกระดับชั้น", "ม.1", "ม.4"};

        //TakeTra M1
        [Header("TakeTraM1")] 
        [SerializeField] private GameObject takeTraM1;

        //TakeTra M4
        [Header("TakeTraM4")] 
        [SerializeField] private GameObject takeTraM4;
        
        //Face Mask
        [Header("Face Mask")] 
        [SerializeField] private GameObject[] faceMask;
        [SerializeField] private Button captureFace;

        [SerializeField] private TextMeshProUGUI skip;
        
        //Sur
        [SerializeField] private RectTransform surM1;
        [SerializeField] private RectTransform surM4;
        [SerializeField] private TextMeshProUGUI studentName;
        

        private int charIndexM1;//Handle M1 Character
        private int charIndexM4;//Handle M4 Character
        private string selectGrade; //Handle grade select

        public static event Action ButtonClickedEvent; 

        private int charcterControl;
        


        private void Awake()
        {
            
            //HidePanel
            chooseCharacterPanelM1.gameObject.SetActive(!true);
            chooseCharacterPanelM4.gameObject.SetActive(!true);
            charactersM1[1].gameObject.SetActive(!true);
            charactersM4[1].gameObject.SetActive(!true);
            uploadPhotoPanel.gameObject.SetActive(!true);
            storyPanelM1.gameObject.SetActive(!true);
            storyPanelM4.gameObject.SetActive(!true);
            BasicAnimation.Instance.HideCharacter();
            foreach (var faces in faceMask)
            {
                faces.gameObject.SetActive(!true);
            }
            studentName.gameObject.SetActive(!true);
            surM1.gameObject.SetActive(!true);
            surM4.gameObject.SetActive(!true);
            takeTraM1.gameObject.SetActive(!true);
            takeTraM4.gameObject.SetActive(!true);
            bgM1.gameObject.SetActive(!true);
            bgM4.gameObject.SetActive(!true);
            
            //Dropdown
            PopulateList();
            start_text.color = Color.red;


            //Set Button
            
            //Face Mask
            captureFace.onClick.AddListener(ShowMask);

            //Character Panel
            nextCharM1.onClick.AddListener(OnNextCharM1Clicked);
            previousCharsM1.onClick.AddListener(OnPreviousCharM1Clicked);
            nextCharM4.onClick.AddListener(OnNextCharM4Clicked);
            previousCharsM4.onClick.AddListener(OnPreviousCharM4Clicked);
            nextPageM1.onClick.AddListener(OnNextPageClicked);
            nextPageM4.onClick.AddListener(OnNextPageClicked);

            //Photo
            toStoryButton.onClick.AddListener(OnToStoryButtonClicked);
        }

        //Face Mask
        private void ShowMask()
        {
            foreach (var faces in faceMask)
            {
                SoundManager.Instance.Play(SoundManager.Sound.Shutter);
                faces.gameObject.SetActive(true);
            }
        }

        //Dropdown
        private void PopulateList()
        {
            dropdown.AddOptions(grade);
        }

        //Start Panel
        
        private void OnStartButtonClickedByM1()
        {
            ButtonClickedEvent?.Invoke();
            chooseCharacterPanelM1.gameObject.SetActive(true);
            startPanel.gameObject.SetActive(false);
        }

        private void OnStartButtonClickedByM4()
        {
            ButtonClickedEvent?.Invoke();
            chooseCharacterPanelM4.gameObject.SetActive(true);
            startPanel.gameObject.SetActive(false);
        }
        
        public void HandleDropDown(int value)
        {
            switch (value)
            {
                case 0:
                    ButtonClickedEvent?.Invoke();
                    start_text.color = Color.red;
                    head.text = "พิธีรับตราออนไลน์";
                    bgM1.gameObject.SetActive(!true);
                    bgM4.gameObject.SetActive(!true);
                    startButton.onClick.RemoveListener(OnStartButtonClickedByM1);
                    startButton.onClick.RemoveListener(OnStartButtonClickedByM4);
                    break;
                case 1:
                    ButtonClickedEvent?.Invoke();
                    head.text = "พิธีรับตราโรงเรียนออนไลน์";
                    bgM1.gameObject.SetActive(true);
                    bgM4.gameObject.SetActive(!true);
                    charcterControl = 1;
                    start_text.color = Color.white;
                    startButton.onClick.AddListener(OnStartButtonClickedByM1);
                    startButton.onClick.RemoveListener(OnStartButtonClickedByM4);
                    selectGrade = "M1";
                    break;
                case 2:
                    ButtonClickedEvent?.Invoke();
                    head.text = "พิธีรับตราเสมาออนไลน์";
                    bgM4.gameObject.SetActive(true);
                    bgM1.gameObject.SetActive(!true);
                    charcterControl = 3;
                    start_text.color = Color.white;
                    startButton.onClick.AddListener(OnStartButtonClickedByM4);
                    startButton.onClick.RemoveListener(OnStartButtonClickedByM1);
                    selectGrade = "M4";
                    break;
            }
        }
        //Start Panel
        
        //Character Panel
        private void HandelCharM1()
        {
            while (true)
            {
                switch (charIndexM1)
                {
                    case -1:
                        charIndexM1 = 1;
                        continue;
                    case 2:
                        charIndexM1 = 0;
                        continue;
                    case 0:
                        charcterControl = 1;
                        charactersM1[1].gameObject.SetActive(!true);
                        charactersM1[0].gameObject.SetActive(true);
                        break;
                    case 1:
                        charcterControl = 2;
                        charactersM1[0].gameObject.SetActive(!true);
                        charactersM1[1].gameObject.SetActive(true);
                        break;
                }

                break;
            }
        }

        private void HandelCharM4()
        {
            while (true)
            {
                switch (charIndexM4)
                {
                    case -1:
                        charIndexM4 = 1;
                        continue;
                    case 2:
                        charIndexM4 = 0;
                        continue;
                    case 0:
                        charcterControl = 3;
                        charactersM4[1].gameObject.SetActive(!true);
                        charactersM4[0].gameObject.SetActive(true);
                        break;
                    case 1:
                        charcterControl = 4;
                        charactersM4[0].gameObject.SetActive(!true);
                        charactersM4[1].gameObject.SetActive(true);
                        break;
                }

                break;
            }
        }

        private void OnNextCharM1Clicked()
        {
            ButtonClickedEvent?.Invoke();
            charIndexM1++;
            HandelCharM1();
        }

        private void OnPreviousCharM1Clicked()
        {
            ButtonClickedEvent?.Invoke();
            charIndexM1--;
            HandelCharM1();
        }

        private void OnNextCharM4Clicked()
        {
            ButtonClickedEvent?.Invoke();
            charIndexM4--;
            HandelCharM4();
        }

        private void OnPreviousCharM4Clicked()
        {
            ButtonClickedEvent?.Invoke();
            charIndexM4--;
            HandelCharM4();
        }
        
        private void OnNextPageClicked()
        {
            ButtonClickedEvent?.Invoke();
            chooseCharacterPanelM1.gameObject.SetActive(false);
            chooseCharacterPanelM4.gameObject.SetActive(false);
            uploadPhotoPanel.gameObject.SetActive(true);
        }
        //Character Panel

        //Story Panel
        private void OnToStoryButtonClicked()
        {
            ButtonClickedEvent?.Invoke();
            uploadPhotoPanel.gameObject.SetActive(false);
            if (charcterControl == 1)
            {
                studentName.text = GetInput.StudentNameM1;
                BasicAnimation.Instance.boyM1.gameObject.SetActive(true);
            }
            else if(charcterControl == 2)
            {
                studentName.text = GetInput.StudentNameM1;
                BasicAnimation.Instance.girlM1.gameObject.SetActive(true);
            }
            else if (charcterControl == 3)
            {
                studentName.text = GetInput.StudentNameM4;
                BasicAnimation.Instance.boyM4.gameObject.SetActive(true);
            }
            else if (charcterControl == 4)
            {
                studentName.text = GetInput.StudentNameM4;
                BasicAnimation.Instance.girlM4.gameObject.SetActive(true);
            }
            
            if (selectGrade == "M1")
            {
                BasicAnimation.Instance.Fade += ShowSurM1;
                takeTraM1.gameObject.SetActive(true);
                storyPanelM1.gameObject.SetActive(true);
                OnlineVideoLoader.Instance.PlayVideM1();
                Invoke("SkipStoryPanel", 30);
                Invoke("ChangTextSkip", 55);
            }
            else
            {
                BasicAnimation.Instance.Fade += ShowSurM4;
                takeTraM4.gameObject.SetActive(true);
                storyPanelM4.gameObject.SetActive(true); 
                OnlineVideoLoader.Instance.PlayVideM4();
                Invoke("SkipStoryPanel", 18);
                Invoke("ChangTextSkip", 70);
            }
        }

        private void ChangTextSkip()
        {
            skip.text = "เข้าร่วมพิธี";
        }

        private void SkipStoryPanel()
        {
            BasicAnimation.Instance.takeTra.gameObject.SetActive(true);
            BasicAnimation.Instance.HideStory += HideStoryPanel;
        }

        private void HideStoryPanel()
        {
            storyPanelM1.gameObject.SetActive(false); 
            storyPanelM4.gameObject.SetActive(false);
        }
        //Story Panel
        
        //Sur Panel
        private void ShowSurM1()
        {
            studentName.gameObject.SetActive(true);
            surM1.gameObject.SetActive(true);
        }
        private void ShowSurM4()
        {
            studentName.gameObject.SetActive(true);
            surM4.gameObject.SetActive(true);
        }
        //Sur Panel
    }
}