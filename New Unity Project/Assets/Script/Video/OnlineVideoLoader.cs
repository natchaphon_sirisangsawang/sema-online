using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
 
public class OnlineVideoLoader : MonoBehaviour
{
     
    public VideoPlayer videoPlayerM1;
    public string videoUrlM1 = "yourvideourl";
    
    public VideoPlayer videoPlayerM4;
    public string videoUrlM4 = "yourvideourl";

    
    public static OnlineVideoLoader Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void PlayVideM4()
    {
        videoPlayerM4.url = videoUrlM4;
        videoPlayerM4.audioOutputMode = VideoAudioOutputMode.AudioSource;
        videoPlayerM4.EnableAudioTrack (0, true);
        videoPlayerM4.Prepare ();
    }
    
    public void PlayVideM1()
    {
        videoPlayerM1.url = videoUrlM1;
        videoPlayerM1.audioOutputMode = VideoAudioOutputMode.AudioSource;
        videoPlayerM1.EnableAudioTrack (0, true);
        videoPlayerM1.Prepare ();
    }
}
