﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using UnityEngine.Networking;


public class DBManager : MonoBehaviour
{
    public RawImage img;
    string imageString;
    // Start is called before the first frame update 
    void getRawImg()
    {
        Texture2D texture = new Texture2D(img.texture.width, img.texture.height);
        texture.SetPixels(texture.GetPixels());
        texture.Apply();
        //image to byte
        byte[] imgByte = texture.EncodeToPNG();
        // get string from byte[]
        imageString = Convert.ToBase64String(imgByte);
    }
    

    public IEnumerator Upload()
    {
        getRawImg();
        WWWForm form = new WWWForm();
        form.AddField("image",imageString);
        
        UnityWebRequest www = UnityWebRequest.Post("http://localhost/unity_backend/sendImageToDB.php",form);
        www.chunkedTransfer = false;
        yield return www.SendWebRequest();
        Debug.Log(www.downloadHandler.text);
    }

}
