﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetInput : MonoBehaviour
{
    [NonSerialized] public static string StudentNameM1;
    [NonSerialized] public static string StudentNameM4;
    public void ReadStringInputM1(string input)
    {
        StudentNameM1 = $"ขอมอบตราโรงเรียนนี้ให้แด่ {input}";
        Debug.Log(StudentNameM1);
    }
    
    public void ReadStringInputM4(string input)
    {
        StudentNameM4 = $"ขอมอบเสมานี้ให้แด่ {input}";
        Debug.Log(StudentNameM4);
    }
}